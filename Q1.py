def start_game(N):
    ans = []
    rule = {3:"Fizz", 5:"Buzz", 7:"Woof"}
    for i in range(1,N+1):
        temp = ""
        for item in rule.keys():
            if i % item ==0:
                temp += rule[item]
        if not temp:
            temp = i
        ans.append(temp)
    return ans

print(start_game(15))
