def str_reversed(words):
    ans = ""
    for i in reversed(range(len(words))):
        ans += words[i]

    return ans

print(str_reversed("xyz123"))
