def test_sum(arr, target):
    arr.sort()
    return test(arr, target)

def test(arr, target):
    if target ==0:
        return True
    elif target <0 or not arr:
        return False
    for i in reversed(range(len(arr))):
        if arr[i] > target:
            return test(arr[:i], target)
        return test(arr, target - arr[i])

print(test_sum([1,2, 5], 11))
print(test_sum([3, 77], 100))
